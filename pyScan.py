import nmap
from termcolor import colored 

class Scanner:
        
    def __init__(self, ip, ports):
        self.nm = nmap.PortScanner()
        self.ip = ip
        self.ports = ports
        
    def scan(self):
        return  self.nm.scan(self.ip, self.ports)
    
    def scanner_info(self):
        return self.nm.scaninfo()
    
    def command_line(self):
        return self.nm.command_line()
    
    def all_host(self):
        return self.nm.all_hosts()
        
    def save(self):
        print (colored("\n***************************** ", 'red', attrs=['bold', 'blink']))
        print (colored("\t CSV FILE ", 'green', attrs=['bold', 'blink']))
        print (colored("***************************** \n", 'red', attrs=['bold', 'blink']))
        print (self.nm.csv())
        print (colored("\n***************************** ", 'red', attrs=['bold', 'blink']))
        print (colored("\t LOCATION ", 'green', attrs=['bold', 'blink']))
        print (colored("***************************** \n", 'red', attrs=['bold', 'blink']))
        print(dir(self.nm))
        print("\n")
        
    def print_info(self):
        print (colored("\n***************************** ", 'red', attrs=['bold', 'blink']))
        print (colored("\t SCAN INFO ", 'green', attrs=['bold', 'blink']))
        print (colored("***************************** \n", 'red', attrs=['bold', 'blink']))
        print(self.scan())
        print (colored("\n***************************** ", 'red', attrs=['bold', 'blink']))
        print (colored("\t COMMAND LINE ", 'green', attrs=['bold', 'blink']))
        print (colored("***************************** \n", 'red', attrs=['bold', 'blink']))
        print(self.command_line())
        print (colored("\n***************************** ", 'red', attrs=['bold', 'blink']))
        print (colored("\t ALL HOST ", 'green', attrs=['bold', 'blink']))
        print (colored("***************************** \n", 'red', attrs=['bold', 'blink']))
        print(self.all_host())

    def print_detailed(self):
        self.scan()
        for host in self.nm.all_hosts():
            print('Host : %s (%s)' % (host, self.nm[host].hostname()))
            print('State : %s' % self.nm[host].state())
            for proto in self.nm[host].all_protocols():
                print('--------------------------------------')
                print('Protocol : %s' % proto)
                
                lport = self.nm[host][proto].keys()
                lport.sort()
            for port in lport:
                if (self.nm[host][proto][port]['state'] == "open"):
                    print('port : %s | State : %s' % (port, self.nm[host][proto][port]['state'])) 
                    print('Name : %s | Version : %s' % (self.nm[host][proto][port]['name'], self.nm[host][proto][port]['version']))
                    print('----------------------------------------------------')
            
    def get_open_ports(self):
        open_ports = []
        self.scan()
        for host in self.nm.all_hosts():
            for proto in self.nm[host].all_protocols():
                lport = self.nm[host][proto].keys()
                lport.sort()
            for port in lport:
                print('----------------------------------------------------')
                if (self.nm[host][proto][port]['state'] == "open"):
                    open_ports.append(port)
        return open_ports          