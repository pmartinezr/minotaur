Minotaur SSH/FTP Bruteforcer GPLv3
Minotaur is a mitological animal of ancient greeks, inspired on other software
called Hydra, this only has two horns, SSH and FTP


Don't use this tool without express consent of taget!!
======================================================

Installation
pip install -r requirements.txt --user


Configuration:
Ports are pre-defined in the minotaur.cfg file, change them if needed

[ports]
ftp =    21
ssh =    22


Usage: main.py [options]

Options:
  -h, --help            show this help message and exit
  -d VICTIM, --dest=VICTIM
                        Destination ip/domain
  -u USER, --user=USER  User
  -U USERSFILE, --Usersfile=USERSFILE
                        User
  -w DICTIONARY, --wordlist=DICTIONARY
                        Dictionary
